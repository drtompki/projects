# Multi-Threaded HTTP Server

**Overview** 
- This project combines previous projects to build a multi-threaded HTTP server. The server adds a thread-safe queue and rwlocks to an HTTP server so it can serve multiple clients simultaneously, ensuring responses conform to a coherent and atomic linearization of client requests. It creates an audit log to identify the linearization.

**Functionality** 
- **Threaded Processing**: Handles multiple clients simultaneously using a thread-pool design.
- **Audit Log**: Produces a log indicating the order of processed requests, output to stderr.
- **Ordering Requirements**: Ensures responses are coherent and atomic with respect to the ordering specified in the audit log.
- **Efficiency**: Balances efficient processing while maintaining order and coherency.
- **Additional Functionality**: No memory leaks, and usage is limited to under 10 MBs.

**Usage** 
The `httpserver` takes two command-line arguments:
- `port`: The port to listen on.
- `threads`: The number of worker threads to use (optional, defaulting to 4).

./httpserver [-t threads] <port>

**Building and Running** 
- **Makefile**: Use the `make` command in the `MultiThreadedServer` directory.
- **Clang-Format**: Format all `.c` and `.h` files according to the provided `.clang-format` file.
- **Execution**: Run the `httpserver` binary with the appropriate command-line arguments.

**Limitations** 
- Cannot use external program execution functions like `system` or `execve`.

**Acknowledgments** 
- This project is part of the CSE 130 course under the Computer Science and Engineering department at UCSC.
