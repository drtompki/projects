#include "asgn2_helper_funcs.h"
#include "connection.h"
#include "queue.h"
#include "request.h"
#include "response.h"
#include "rwlock.h"

#include <stdio.h>
#include <err.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/file.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE         4096
#define SERVER_ARGS         "t:"
#define DEFAULT_NUM_THREADS 4

rwlock_t *rwlock;
queue_t *requestQueue;

void writeAuditLog(const char *operation, conn_t *conn, int statusCode) {
    char *uri = conn_get_uri(conn);
    char *requestId = conn_get_header(conn, "Request-Id");
    if (!uri)
        uri = "unknown_uri";
    if (!requestId)
        requestId = "0";

    fprintf(stderr, "%s,%s,%d,%s\n", operation, uri, statusCode, requestId);
}

void get_request(conn_t *conn) {
    char *uri = conn_get_uri(conn);
    const Response_t *res = NULL;
    int fileDescriptor = open(uri, O_RDONLY);
    if (fileDescriptor < 0) {
        res = (errno == EACCES)   ? &RESPONSE_FORBIDDEN
              : (errno == ENOENT) ? &RESPONSE_NOT_FOUND
                                  : &RESPONSE_INTERNAL_SERVER_ERROR;
        conn_send_response(conn, res);
        writeAuditLog("GET", conn,
            res == &RESPONSE_FORBIDDEN   ? 403
            : res == &RESPONSE_NOT_FOUND ? 404
                                         : 500);
        return;
    }

    reader_lock(rwlock);
    struct stat fileInfo;
    fstat(fileDescriptor, &fileInfo);
    off_t fileSize = fileInfo.st_size;

    if (S_ISDIR(fileInfo.st_mode)) {
        res = &RESPONSE_FORBIDDEN;
        conn_send_response(conn, res);
        reader_unlock(rwlock);
        if (fileDescriptor >= 0)
            close(fileDescriptor);
        writeAuditLog("GET", conn, 403);
        return;
    }

    res = conn_send_file(conn, fileDescriptor, fileSize);
    reader_unlock(rwlock);
    if (fileDescriptor >= 0)
        close(fileDescriptor);
    writeAuditLog("GET", conn, res != NULL ? 200 : 500);
}

void put_request(conn_t *conn) {
    char *uri = conn_get_uri(conn);
    const Response_t *res = NULL;

    writer_lock(rwlock);
    bool fileExisted = access(uri, F_OK) == 0;
    int fileDescriptor = open(uri, O_CREAT | O_WRONLY, 0600);

    if (fileDescriptor < 0) {
        res = (errno == EACCES || errno == EISDIR || errno == ENOENT)
                  ? &RESPONSE_FORBIDDEN
                  : &RESPONSE_INTERNAL_SERVER_ERROR;
        conn_send_response(conn, res);
        writeAuditLog("PUT", conn, res == &RESPONSE_FORBIDDEN ? 403 : 500);
        writer_unlock(rwlock);
        return;
    }

    ftruncate(fileDescriptor, 0);
    res = conn_recv_file(conn, fileDescriptor);

    if (res == NULL) {
        res = fileExisted ? &RESPONSE_OK : &RESPONSE_CREATED;
    }
    conn_send_response(conn, res);
    writeAuditLog("PUT", conn, res == &RESPONSE_OK ? 200 : (res == &RESPONSE_CREATED ? 201 : 500));
    writer_unlock(rwlock);
    if (fileDescriptor >= 0)
        close(fileDescriptor);
}

void unsupp_request(conn_t *conn) {
    conn_send_response(conn, &RESPONSE_NOT_IMPLEMENTED);
    writeAuditLog("UNSUPPORTED", conn, 501);
}

void processClientConn(int connectionFd) {
    conn_t *conn = conn_new(connectionFd);
    const Response_t *response = conn_parse(conn);

    if (response != NULL) {
        conn_send_response(conn, response);
    } else {
        const Request_t *request = conn_get_request(conn);
        if (request == &REQUEST_GET) {
            get_request(conn);
        } else if (request == &REQUEST_PUT) {
            put_request(conn);
        } else {
            unsupp_request(conn);
        }
    }
    conn_delete(&conn);
}

void *serverThread() {
    while (1) {
        uintptr_t connectionFd = -1;
        queue_pop(requestQueue, (void **) &connectionFd);
        if (connectionFd != (uintptr_t) -1) {
            processClientConn(connectionFd);
            close(connectionFd);
        }
    }
}

void printUsage(const char *programName) {
    fprintf(stderr, "Usage: %s [-t numThreads] <port>\n", programName);
}

size_t getPortNum(char **argv, int index) {
    char *end = NULL;
    size_t port = strtoul(argv[index], &end, 10);
    if (end && *end != '\0') {
        warnx("Invalid port number: %s", argv[index]);
        return 0;
    }
    return port;
}

int getThreadNum(const char *arg) {
    int threadCount = atoi(arg);
    if (threadCount < 0) {
        fprintf(stderr, "Invalid thread size.\n");
        exit(EXIT_FAILURE);
    }
    return threadCount;
}

int initServer(size_t port, int threadCount) {
    signal(SIGPIPE, SIG_IGN);
    Listener_Socket serverSocket;
    listener_init(&serverSocket, port);

    requestQueue = queue_new(threadCount);
    rwlock = rwlock_new(READERS, 0);

    pthread_t threads[threadCount];
    for (int i = 0; i < threadCount; i++) {
        pthread_create(&threads[i], NULL, serverThread, NULL);
    }

    while (1) {
        uintptr_t connectionFd = listener_accept(&serverSocket);
        queue_push(requestQueue, (void *) connectionFd);
    }

    return EXIT_SUCCESS;
}

int main(int argc, char **argv) {
    int option;
    int numThreads = DEFAULT_NUM_THREADS;

    while ((option = getopt(argc, argv, SERVER_ARGS)) != -1) {
        switch (option) {
        case 't': numThreads = getThreadNum(optarg); break;
        default: printUsage(argv[0]); return EXIT_FAILURE;
        }
    }

    size_t port = getPortNum(argv, optind);
    if (port == 0) {
        printUsage(argv[0]);
        return EXIT_FAILURE;
    }

    return initServer(port, numThreads);
}
