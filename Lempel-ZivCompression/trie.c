#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "trie.h"
#include "code.h"

TrieNode *trie_node_create(uint16_t code) {
    TrieNode *node = malloc(sizeof(TrieNode));
    if (node == NULL) {
        return NULL;
    }
    node->code = code;
    for (int i = 0; i < ALPHABET; i++) {
        node->children[i] = NULL;
    }
    return node;
}

void trie_node_delete(TrieNode *n) {
    free(n);
}

TrieNode *trie_create(void) {
    TrieNode *root = trie_node_create(EMPTY_CODE);
    return root;
}

void trie_reset(TrieNode *root) {
    for (int i = 0; i < ALPHABET; i++) {
        if (root->children[i] != NULL) {
            trie_delete(root->children[i]);
            root->children[i] = NULL;
        }
    }
}

void trie_delete(TrieNode *n) {
    for (int i = 0; i < ALPHABET; i++) {
        if (n->children[i] != NULL) {
            trie_delete(n->children[i]);
            n->children[i] = NULL;
        }
    }
    trie_node_delete(n);
}

TrieNode *trie_step(TrieNode *n, uint8_t sym) {
    if (n == NULL) {
        return NULL;
    }
    return n->children[sym];
}
