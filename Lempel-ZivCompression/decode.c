#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>

#include "io.h"
#include "code.h"
#include "word.h"

void print_help(void) {
    printf("SYNOPSIS\n   Decompresses files with the LZ78 decompression algorithm.\n"
           "Used with files compressed with the corresponding encoder.\n"
           "USAGE\n   ./decode [-vh] [-i input] [-o output]\n"
           "OPTIONS\n"
           "      -v          Display decompression statistics\n"
           "      -i input    Specify input to decompress (stdin by default)\n"
           "      -o output   Specify output of decompressed input (stdout by default)\n"
           "      -h          Display program usage\n");
}

int bitlen(uint16_t in) {
    int bits = 0;
    while (in != 0) {
        bits++;
        in >>= 1;
    }
    return bits;
}

int main(int argc, char *argv[]) {
    // Command line options
    char *input_file = NULL;
    char *output_file = NULL;
    bool verbose = false;

    // Parse command line options
    int opt;
    while ((opt = getopt(argc, argv, "vi:o:")) != -1) {
        switch (opt) {
        case 'v': verbose = 1; break;
        case 'i': input_file = optarg; break;
        case 'o': output_file = optarg; break;
        case 'h': print_help(); return 0;
        default: print_help(); return 0;
        }
    }

    // Open infile
    int infile = STDIN_FILENO;
    if (input_file != NULL) {
        infile = open(input_file, O_RDONLY);
        if (infile < 0) {
            perror("open input file");
            exit(EXIT_FAILURE);
        }
    }

    // Open outfile
    int outfile = STDOUT_FILENO;
    if (output_file != NULL) {
        outfile = open(output_file, O_WRONLY | O_CREAT | O_TRUNC, 0644);
        if (outfile < 0) {
            perror("open");
            exit(EXIT_FAILURE);
        }
    }

    struct stat file_stats;
    if (fstat(infile, &file_stats) < 0) {
        fprintf(stderr, "Error: Failed to read input file stats.\n");
        exit(EXIT_FAILURE);
    }

    // Read header from infile
    FileHeader header;
    read_header(infile, &header);

    // Create and initialize word table
    WordTable *table = wt_create();
    for (int i = 0; i < MAX_CODE; i++) {
        table[i] = NULL;
    }
    table[EMPTY_CODE] = word_create(NULL, 0);

    // Initialize current and next codes
    uint16_t curr_code = 0;
    uint16_t next_code = START_CODE;
    uint8_t curr_sym = 0;

    while (read_pair(infile, &curr_code, &curr_sym, bitlen(next_code))) {

        if (curr_code == STOP_CODE) {
            break;
        }

        Word *curr_word = table[curr_code];
        Word *new_word = word_append_sym(curr_word, curr_sym);
        table[next_code] = new_word;
        write_word(outfile, new_word);
        next_code++;
        if (next_code == MAX_CODE) {
            wt_reset(table);
            next_code = START_CODE;
        }
    }

    flush_words(outfile);
    wt_delete(table);

    // Print statistics if verbose flag is set
    if (verbose) {
        // Get compressed file size
        off_t uncompressed_size = lseek(outfile, 0, SEEK_CUR);
        if (uncompressed_size < 0) {
            fprintf(stderr, "Error: failed to get compressed file size: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        // Get uncompressed file size
        off_t compressed_size = file_stats.st_size;

        // Calculate space saving
        double space_saving
            = 100.0 * (1.0 - ((double) compressed_size / (double) uncompressed_size));

        // Print compression statistics to stderr
        fprintf(stderr, "Compressed file size: %ld bytes\n", compressed_size);
        fprintf(stderr, "Uncompressed file size: %ld bytes\n", uncompressed_size);
        fprintf(stderr, "Space saving: %.2f%%\n", space_saving);
    }

    // Close the input and output files
    if (infile != STDIN_FILENO) {
        if (close(infile) != 0) {
            fprintf(stderr, "Error: Failed to close input file.\n");
            exit(EXIT_FAILURE);
        }
    }

    if (outfile != STDOUT_FILENO) {
        if (close(outfile) != 0) {
            fprintf(stderr, "Error: Failed to close output file.\n");
            exit(EXIT_FAILURE);
        }
    }

    return EXIT_SUCCESS;
}
