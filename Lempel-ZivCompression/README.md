# Lempel-Ziv Compression

**Overview** 
- This project involves implementing Lempel-Ziv (LZ78) compression and decompression algorithms. The objective is to create two programs, `encode` and `decode`, which perform LZ78 compression and decompression, respectively. These programs are designed to handle both text and binary files and operate efficiently on different system architectures.

**Functionality** 
- **Compression and Decompression**: Implements LZ78 algorithm for compressing and decompressing files.
- **Support for Various File Types**: Capable of processing both text and binary files.
- **Cross-platform Compatibility**: Works on both little and big endian systems.
- **Efficient I/O**: Utilizes 4KB blocks for reading and writing, ensuring efficiency.
- **Variable Bit-Length Codes**: Supports variable bit-lengths for codes in the LZ78 algorithm.

**Usage** 
The `encode` and `decode` programs accept the following command-line options:
- `-v`: Print compression/decompression statistics to stderr.
- `-i <input>`: Specify the input file (stdin by default).
- `-o <output>`: Specify the output file (stdout by default).

./encode [options]
./decode [options]

**Building and Running** 
- **Makefile**: Use the `make` command in the project directory.
- **Options**: Use `-v`, `-i`, and `-o` flags to specify verbose mode, input file, and output file, respectively.
- **Execution**: Run the `encode` and `decode` binaries with the appropriate options.

**Limitations** 
- No external program execution functions like `system` or `execve` allowed.

**Acknowledgments** 
- This project is part of the CSE 13s course under the Computer Science and Engineering department at UCSC.


