#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>

#include "io.h"
#include "code.h"
#include "trie.h"

void print_help(void) {
    printf("SYNOPSIS\n   Compresses files using the LZ78 compression algorithm.\n"
           "Compressed files are decompressed with the corresponding decoder.\n"
           "USAGE\n   ./encode [-vh] [-i input] [-o output]\n"
           "OPTIONS\n"
           "   -v          Display compression statistics.\n"
           "   -i input    Specify input to compress (stdin by default).\n"
           "   -o output   Specify output of compressed input (stdout by default).\n"
           "   -h          Display program help and usage\n");
}

int main(int argc, char **argv) {
    char *input_file = NULL;
    char *output_file = NULL;
    bool verbose = false;

    // Parse command line options
    int opt;
    while ((opt = getopt(argc, argv, "vi:o:")) != -1) {
        switch (opt) {
        case 'v': verbose = true; break;
        case 'i': input_file = optarg; break;
        case 'o': output_file = optarg; break;
        case 'h': print_help(); return 0;
        default: print_help(); return 0;
        }
    }

    // Open input file
    int infile = STDIN_FILENO;
    if (input_file != NULL) {
        infile = open(input_file, O_RDONLY);
        if (infile < 0) {
            perror("open input file");
            exit(EXIT_FAILURE);
        }
    }

    // Open output file if specified
    int outfile = STDOUT_FILENO;
    if (output_file != NULL) {
        outfile = open(output_file, O_WRONLY | O_CREAT | O_TRUNC, 0644);
        if (outfile < 0) {
            perror("open");
            exit(EXIT_FAILURE);
        }
    }

    struct stat file_stats;
    if (fstat(infile, &file_stats) < 0) {
        fprintf(stderr, "Error: Failed to read input file stats.\n");
        exit(EXIT_FAILURE);
    }

    // Initialize the file header.
    FileHeader header;
    header.magic = MAGIC;
    header.protection = file_stats.st_mode & 0777;

    write_header(outfile, &header);

    //Create a trie
    TrieNode *root = trie_create();
    TrieNode *curr_node = root;

    uint16_t next_code = START_CODE;
    TrieNode *prev_node = NULL;
    int prev_sym = 0;
    int bitlen = 1;

    //Read input symbols and encode
    uint8_t curr_sym;
    while (read_sym(infile, &curr_sym)) {

        if (next_code == (1 << bitlen)) {
            bitlen++;
        }
        TrieNode *next_node = trie_step(curr_node, curr_sym);
        if (next_node != NULL) {
            prev_node = curr_node;
            curr_node = next_node;
        } else {
            write_pair(outfile, curr_node->code, curr_sym, bitlen);
            curr_node->children[curr_sym] = trie_node_create(next_code);
            curr_node = root;
            next_code++;
            if (next_code == MAX_CODE) {
                trie_reset(root);
                bitlen = 1;
            }
        }
        prev_sym = curr_sym;
    }

    //Write the final code
    if (curr_node != root) {
        write_pair(outfile, prev_node->code, prev_sym, bitlen);
        next_code = (next_code + 1) % MAX_CODE;
    }

    write_pair(outfile, STOP_CODE, 0, bitlen);
    flush_pairs(outfile);

    if (verbose) {
        // Get compressed file size
        off_t compressed_size = lseek(outfile, 0, SEEK_CUR);
        if (compressed_size < 0) {
            fprintf(stderr, "Error: failed to get compressed file size: %s\n", strerror(errno));
            exit(EXIT_FAILURE);
        }

        // Get uncompressed file size
        off_t uncompressed_size = file_stats.st_size;

        // Calculate space saving
        double space_saving
            = 100.0 * (1.0 - ((double) compressed_size / (double) uncompressed_size));

        // Print compression statistics to stderr
        fprintf(stderr, "Compressed file size: %ld bytes\n", compressed_size);
        fprintf(stderr, "Uncompressed file size: %ld bytes\n", uncompressed_size);
        fprintf(stderr, "Space saving: %.2f%%\n", space_saving);
    }

    // Close the input and output files
    if (infile != STDIN_FILENO) {
        if (close(infile) != 0) {
            fprintf(stderr, "Error: Failed to close input file.\n");
            exit(EXIT_FAILURE);
        }
    }

    if (outfile != STDOUT_FILENO) {
        if (close(outfile) != 0) {
            fprintf(stderr, "Error: Failed to close output file.\n");
            exit(EXIT_FAILURE);
        }
    }

    // Exit
    return EXIT_SUCCESS;
}
