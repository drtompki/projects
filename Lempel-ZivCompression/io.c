#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "io.h"
#include "code.h"
#include "endian.h"

// global variables to count symbols and bits
uint64_t total_syms = 0;
uint64_t total_bits = 0;

static uint8_t pair_buf[BLOCK];
static int pair_index = 0;

static uint8_t word_buf[BLOCK];
static int word_index = 0;

int read_bytes(int infile, uint8_t *buf, int to_read) {
    int bytes_read = 0;
    while (bytes_read < to_read) {
        int bytes = read(infile, buf + bytes_read, to_read - bytes_read);
        if (bytes < 0) {
            return -1;
        } else if (bytes == 0) {
            break;
        } else {
            bytes_read += bytes;
        }
    }
    return bytes_read;
}

int write_bytes(int outfile, uint8_t *buf, int to_write) {
    int bytes_writen = 0;
    while (bytes_writen < to_write) {
        int bytes = write(outfile, buf + bytes_writen, to_write - bytes_writen);
        if (bytes < 0) {
            return -1;
        } else {
            bytes_writen += bytes;
        }
    }
    return bytes_writen;
}

void read_header(int infile, FileHeader *header) {
    int num_read = read_bytes(infile, (uint8_t *) header, sizeof(FileHeader));
    if (num_read != sizeof(FileHeader)) {
        fprintf(stderr, "Error: Could not read header\n");
        exit(1);
    }
    if (!little_endian()) {
        header->magic = swap32(header->magic);
        header->protection = swap16(header->protection);
    }
    if (header->magic != MAGIC) {
        fprintf(stderr, "Error: Invalid magic number\n");
        exit(1);
    }
}

void write_header(int outfile, FileHeader *header) {
    if (!little_endian()) {
        header->magic = swap32(header->magic);
        header->protection = swap16(header->protection);
    }
    int num_written = write_bytes(outfile, (uint8_t *) header, sizeof(FileHeader));
    if (num_written != sizeof(FileHeader)) {
        fprintf(stderr, "Error: Could not write header\n");
        exit(1);
    }
}

bool read_sym(int infile, uint8_t *sym) {
    static int buf_len = 0;
    if (word_index >= buf_len) {
        buf_len = read_bytes(infile, word_buf, BLOCK);
        word_index = 0;
        if (buf_len == 0) {
            return false;
        }
    }
    *sym = word_buf[word_index++];
    total_syms++;
    return true;
}

void write_pair(int outfile, uint16_t code, uint8_t sym, int bitlen) {
    // Write code
    for (int i = 0; i < bitlen; i++) {
        int bit = code & (1 << i);
        if (bit) {
            pair_buf[pair_index / 8] |= (1 << (pair_index % 8));
        }
        pair_index++;
        if (pair_index == BLOCK * 8) {
            flush_pairs(outfile);
        }
    }

    // Write symbol
    for (int i = 0; i < 8; i++) {
        int bit = sym & (1 << i);
        if (bit) {
            pair_buf[pair_index / 8] |= (1 << (pair_index % 8));
        }
        pair_index++;
        if (pair_index == BLOCK * 8) {
            flush_pairs(outfile);
        }
    }
}

void flush_pairs(int outfile) {
    if (pair_index == 0) {
        return;
    }

    size_t num_written = write(outfile, pair_buf, (pair_index + 7) / 8);
    if (num_written < (size_t) ((pair_index + 7) / 8)) {
        fprintf(stderr, "Error flushing pairs to output file.\n");
        exit(EXIT_FAILURE);
    }

    pair_index = 0;
}

bool read_pair(int infile, uint16_t *code, uint8_t *sym, int bitlen) {
    if (pair_index == 0) {
        ssize_t num_read = read(infile, pair_buf, BLOCK);
        if (num_read == 0) {
            return false;
        }
        pair_index = 0;
    }

    // Read code
    *code = 0;
    for (int i = 0; i < bitlen; i++) {
        if (pair_buf[pair_index / 8] & (1 << (pair_index % 8))) {
            *code |= (1 << i);
        }
        pair_index++;
        if (pair_index == BLOCK * 8) {
            pair_index = 0;
        }
    }

    // Read symbol
    *sym = 0;
    for (int i = 0; i < 8; i++) {
        if (pair_buf[pair_index / 8] & (1 << (pair_index % 8))) {
            *sym |= (1 << i);
        }
        pair_index++;
        if (pair_index == BLOCK * 8) {
            pair_index = 0;
        }
    }

    return (*code != STOP_CODE);
}

void write_word(int outfile, Word *w) {
    for (uint32_t i = 0; i < w->len; i++) {
        word_buf[word_index] = w->syms[i];
        word_index++;

        if (word_index == BLOCK) {
            if (write_bytes(outfile, word_buf, BLOCK) != BLOCK) {
                perror("write word");
                exit(EXIT_FAILURE);
            }

            flush_words(outfile);
        }
    }
}

void flush_words(int outfile) {
    if (word_index > 0) {
        if (write_bytes(outfile, word_buf, word_index) != word_index) {
            perror("flush words");
            exit(EXIT_FAILURE);
        }
        word_index = 0;
    }
}
