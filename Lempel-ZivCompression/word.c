#include <stdlib.h>
#include <string.h>

#include "word.h"
#include "code.h"

Word *word_create(uint8_t *syms, uint32_t len) {
    Word *w = (Word *) malloc(sizeof(Word));

    w->syms = (uint8_t *) malloc(len * sizeof(uint8_t));
    if (w->syms == NULL) {
        free(w);
        return NULL;
    }
    for (uint32_t i = 0; i < len; i++) {
        w->syms[i] = syms[i];
    }
    w->len = len;
    return w;
}

Word *word_append_sym(Word *w, uint8_t sym) {
    Word *new_word = (Word *) malloc(sizeof(Word));
    if (new_word == NULL) {
        return NULL;
    }
    uint32_t new_len = w->len + 1;
    new_word->syms = (uint8_t *) malloc(new_len * sizeof(uint8_t));
    if (new_word->syms == NULL) {
        return NULL;
    }
    memcpy(new_word->syms, w->syms, w->len);
    new_word->syms[new_len - 1] = sym;
    new_word->len = new_len;
    return new_word;
}

void word_delete(Word *w) {
    if (w != NULL) {
        free(w->syms);
        free(w);
    }
}

WordTable *wt_create(void) {
    WordTable *wt = (WordTable *) malloc(MAX_CODE * sizeof(Word));
    if (wt == NULL) {
        return NULL;
    }

    wt[EMPTY_CODE] = word_create(NULL, 0);
    for (uint16_t i = 0; i < MAX_CODE; i++) {
        if (i != EMPTY_CODE) {
            wt[i] = NULL;
        }
    }
    return wt;
}

void wt_reset(WordTable *wt) {
    for (uint16_t i = 0; i < MAX_CODE; i++) {
        if (i != EMPTY_CODE && wt[i] != NULL) {
            word_delete(wt[i]);
            wt[i] = NULL;
        }
    }
}

void wt_delete(WordTable *wt) {
    if (wt != NULL) {
        wt_reset(wt);
        free(wt);
    }
}
