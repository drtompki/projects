# Dakota Tompkins CS Projects

**Overview** 
- This repository contains three projects developed as part of my courses at the University of California, Santa Cruz. These projects demonstrate advanced concepts in systems programming, including caching algorithms, multi-threaded server design, and data compression techniques.

## Projects

### 1. Caching Simulator
**Description**: Implements a caching simulator with different eviction policies to demonstrate effective memory management and optimization techniques.
- **Key Concepts**: Cache management, eviction policies (FIFO, LRU, Clock), and memory optimization.
- **Languages & Tools**: C, Makefile.

### 2. Multi-Threaded HTTP Server
**Description**: A multi-threaded HTTP server capable of handling multiple client requests concurrently, ensuring coherent and atomic processing.
- **Key Concepts**: Multi-threading, thread synchronization, HTTP protocol, and server design.
- **Languages & Tools**: C, Makefile, HTTP.

### 3. Lempel-Ziv Compression
**Description**: Implementation of the LZ78 compression and decompression algorithms, supporting both text and binary files.
- **Key Concepts**: Data compression, Lempel-Ziv algorithm, binary file handling.
- **Languages & Tools**: C, Makefile.

## Skills Demonstrated
- **Systems Programming**: Proficiency in low-level programming concepts and efficient resource management.
- **Algorithm Implementation**: Ability to implement and understand complex algorithms.
- **Concurrent Programming**: Experience in writing multi-threaded code with an emphasis on thread safety and synchronization.
- **Networking and HTTP**: Understanding of basic networking concepts and HTTP server design.
- **Data Compression Techniques**: Knowledge of data encoding and compression algorithms.

## Setup and Usage
- Each project directory contains its own README with detailed instructions for building and running the projects. Refer to the individual README files for more information.
