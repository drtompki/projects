#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define MAX_LINE_LENGTH 256
#define MAX_ITEMS       1024

char itemsEverInCache[MAX_ITEMS][MAX_LINE_LENGTH];
int itemCount = 0;

typedef struct CacheItem {
    char data[MAX_LINE_LENGTH];
    bool referenced;
    struct CacheItem *next, *prev;
} CacheItem;

typedef struct {
    CacheItem *head, *tail, *hand;
    int size, capacity;
    char policy;
    int compulsoryMisses, capacityMisses;
} Cache;

Cache *initializeCache(int size, char policy) {
    Cache *cache = (Cache *) malloc(sizeof(Cache));
    if (!cache)
        return NULL;

    cache->head = cache->tail = cache->hand = NULL;
    cache->size = 0;
    cache->capacity = size;
    cache->policy = policy;
    cache->compulsoryMisses = cache->capacityMisses = 0;

    return cache;
}

bool isItemEverInCache(const char *item) {
    for (int i = 0; i < itemCount; i++) {
        if (strcmp(itemsEverInCache[i], item) == 0) {
            return true;
        }
    }
    return false;
}

void addItemToEverInCache(const char *item) {
    if (itemCount < MAX_ITEMS) {
        // printf("Adding new item to ever in cache: %s\n", item);
        strcpy(itemsEverInCache[itemCount++], item);
    }
}

void addToCache(Cache *cache, const char *item) {
    CacheItem *newItem = (CacheItem *) malloc(sizeof(CacheItem));
    if (!newItem)
        return;

    strcpy(newItem->data, item);
    newItem->referenced = false;

    // Add to the front of the list for LRU
    if (cache->policy == 'L') {
        newItem->next = cache->head;
        newItem->prev = NULL;
        if (cache->head) {
            cache->head->prev = newItem;
        }
        cache->head = newItem;
        if (!cache->tail) {
            cache->tail = newItem;
        }
    } else {
        newItem->next = NULL;
        newItem->prev = cache->tail;
        if (cache->tail) {
            cache->tail->next = newItem;
        }
        cache->tail = newItem;
        if (!cache->head) {
            cache->head = newItem;
        }
    }
    cache->size++;
}

void moveToHead(Cache *cache, CacheItem *item) {
    // Move to front for LRU
    if (item == cache->head) {
        return;
    }

    if (item->prev)
        item->prev->next = item->next;
    if (item->next)
        item->next->prev = item->prev;

    if (item == cache->tail) {
        cache->tail = item->prev;
    }

    item->next = cache->head;
    item->prev = NULL;
    if (cache->head) {
        cache->head->prev = item;
    }
    cache->head = item;
}

bool isHit(Cache *cache, const char *item) {
    CacheItem *current = cache->head;

    while (current != NULL) {
        if (strcmp(current->data, item) == 0) {
            if (cache->policy == 'C') {
                current->referenced = true;
            }
            if (cache->policy == 'L') {
                moveToHead(cache, current);
            }
            //printf("Item '%s' is a Hit\n", item);
            return true;
        }
        current = current->next;
    }
    //printf("Item '%s' is a MISS\n", item);
    return false;
}

void evictFromCache(Cache *cache) {
    if (cache->size == 0)
        return;

    CacheItem *toEvict = NULL;

    // Evict based on policy
    if (cache->policy == 'F') { // FIFO
        toEvict = cache->head;
        cache->head = toEvict->next;
        if (cache->head)
            cache->head->prev = NULL;
    } else if (cache->policy == 'L') { // LRU
        toEvict = cache->tail;
        cache->tail = toEvict->prev;
        if (cache->tail)
            cache->tail->next = NULL;
    } else if (cache->policy == 'C') { // Clock
        if (!cache->hand)
            cache->hand = cache->head;
        while (cache->hand && cache->hand->referenced) {
            cache->hand->referenced = false;
            cache->hand = cache->hand->next ? cache->hand->next : cache->head;
        }
        toEvict = cache->hand;
        cache->hand = toEvict->next ? toEvict->next : cache->head;
    }

    if (toEvict) {
        if (toEvict == cache->head)
            cache->head = toEvict->next;
        if (toEvict == cache->tail)
            cache->tail = toEvict->prev;
        if (toEvict->next)
            toEvict->next->prev = toEvict->prev;
        if (toEvict->prev)
            toEvict->prev->next = toEvict->next;

        free(toEvict);
        cache->size--;
    }
}

void accessCache(Cache *cache, const char *item) {
    //printf("Processing item: %s\n", item); // Debug print

    bool isCompulsory = !isItemEverInCache(item);

    if (isCompulsory) {
        addItemToEverInCache(item);
        cache->compulsoryMisses++;
    }

    if (isHit(cache, item)) {
        printf("HIT\n");
    } else {
        printf("MISS\n");
        if (cache->size >= cache->capacity) {
            evictFromCache(cache);
        }
        addToCache(cache, item);
        if (!isCompulsory) {
            cache->capacityMisses++;
        }
    }
}

void printSummary(const Cache *cache) {
    printf("%d %d\n", cache->compulsoryMisses, cache->capacityMisses);
}

void freeCache(Cache *cache) {
    while (cache->head) {
        CacheItem *temp = cache->head;
        cache->head = cache->head->next;
        free(temp);
    }
    free(cache);
}

int main(int argc, char *argv[]) {
    int cacheSize = 3;
    char policy = 'F';

    if (argc < 2) {
        fprintf(stderr, "Error: Invalid arguments\n");
        return 1;
    }

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-N") == 0 && i + 1 < argc) {
            cacheSize = atoi(argv[++i]);
        } else if (argv[i][0] == '-') {
            policy = argv[i][1];
        }
    }

    Cache *myCache = initializeCache(cacheSize, policy);
    if (!myCache) {
        fprintf(stderr, "Error initializing cache\n");
        return 1;
    }

    char line[MAX_LINE_LENGTH];
    while (fgets(line, sizeof(line), stdin)) {
        line[strcspn(line, "\n")] = 0;
        if (line[0] == '\0') {
            break;
        }
        accessCache(myCache, line);
    }

    printSummary(myCache);
    freeCache(myCache);

    return 0;
}
