# Caching Simulator

**Overview** 
- This project implements a caching simulator as part of CSE 130: Principles of Computer Systems Design. The caching simulator is designed to demonstrate the implementation and testing of real caching algorithms, focusing on reducing disk reads and redundant work.

**Features** 
- **Cache Policies**: Supports First-In-First-Out (FIFO), Least-Recently-Used (LRU), and Clock eviction policies.
- **Continuous Input**: Accepts items from standard input (stdin) continuously until it is closed.
- **Hit/Miss Detection**: After each lookup, the program outputs whether the accessed item is a HIT or a MISS. 
- **Cache Updates**: On a MISS, the item is added to the cache, and an item may be evicted based on the specified policy.
- **Summary Statistics**: Provides a summary of compulsory and capacity misses after the stdin is closed.

**Usage** 
The caching simulator, named `cacher`, accepts two command-line arguments:
- `size`: The size of your cache.
- `policy`: The policy your cache will follow (`-F` for FIFO, `-L` for LRU, `-C` for Clock). Defaults to FIFO if no policy is given.

./cacher [-N size] <policy>

**Building and Running** 
- **Makefile**: Use the `make` command in the `CachingSimulator` directory to build the `cacher`.
- **Clang-Format**: Ensure all `.c` and `.h` files are formatted according to the provided `.clang-format` file.
- **Execution**: Run the `cacher` binary with the appropriate command-line arguments.

**Limitations** 
- External program execution functions like `system` or `execve` are not allowed.

**Acknowledgments** 
- This project is a part of the CSE 130 course under the Computer Science and Engineering department at UCSC.
